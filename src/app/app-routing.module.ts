import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RecipesComponent} from './recipes/recipes.component';
import {ShoppingListComponent} from './shopping-list/shopping-list.component';
import {RecepieStartComponent} from "./recipes/recepie-start/recepie-start.component";
import {RecipeDetailsComponent} from "./recipes/recipe-details/recipe-details.component";
import {RecepieEditComponent} from "./recipes/recepie-edit/recepie-edit.component";

const appRoutes: Routes = [
  { path: '', redirectTo: '/recipes', pathMatch: 'full' },
  { path: 'recipes', component: RecipesComponent, children: [
      { path: '', component: RecepieStartComponent},
      { path: 'new', component: RecepieEditComponent},
      { path: ':id', component: RecipeDetailsComponent},
      { path: ':id/edit', component: RecepieEditComponent},
    ]},
  { path: 'shopping-list', component: ShoppingListComponent},
]
@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

import {Recipe} from './recipe.model';
import {EventEmitter, Injectable} from '@angular/core';
import {Ingredient} from '../shared/ingredient.model';
import {ShoppingListService} from "../shopping-list/shoppingListService";

@Injectable()
export class RecepiesService {
  // creating Event
  // recepieCreated = new EventEmitter<Recipe>(); // commenting as using Subject() observable
  // ----------
  // making private so that other can not use directly
   private recepies: Recipe[] = [
    new Recipe('Test Recpie', 'This is my test rcepie', 'https://ichef.bbci.co.uk/food/ic' +
      '/food_16x9_832/recipes/pumpkinsoup_89904_16x9.jpg', [
      new Ingredient('Bean', 2),
      new Ingredient('tomato', 5)
    ]),
    new Recipe('Another Test Recpie', 'This is my test rcepie', 'https://ichef.bbci.co.uk/food/ic' +
      '/food_16x9_832/recipes/pumpkinsoup_89904_16x9.jpg', [
      new Ingredient('Onion', 2),
      new Ingredient('Capsicum', 5)
    ])
  ];
   constructor(private slService: ShoppingListService) {}
   // getRecepies() is created to access the private array
   getRecepies() {
     // slice() will return copy of above created array i.e Recepie[]
     // slice() will return a cop of array not an original array
     return this.recepies.slice();
   }
   getRecepie(index: number) {
     return this.recepies[index];
   }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
      this.slService.addIngredientsToShoppingFromRecepie(ingredients);
   }
}

import {Component, Input, OnInit} from '@angular/core';
import {Recipe} from '../recipe.model';
import {RecepiesService} from '../recepies.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css']
})
export class RecipeDetailsComponent implements OnInit {
  // removing @Input as adding Routing so cant take input like this
   recepieDetail: Recipe;
   id: number;
   // @Input() recepieDetail: Recipe;
  constructor(private recepieService: RecepiesService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params.id;
          this.recepieDetail = this.recepieService.getRecepie(this.id);
        }
      );
  }

  onAddToShoppingList() {
     this.recepieService.addIngredientsToShoppingList(this.recepieDetail.ingredients);
  }

  onEdit() {
    // both are right added in app.module.ts
   // this.router.navigate(['edit'], {relativeTo: this.route});
    this.router.navigate(['../', this.id , 'edit'], {relativeTo: this.route});
  }
}

import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Recipe} from '../../recipe.model';
import {RecepiesService} from '../../recepies.service';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.css']
})
export class RecipeItemComponent implements OnInit {
  // to get from outside component
   @Input()  recepie: Recipe;
   @Input() index: number;
  // commented as we are using cross component communication using service
   // @Output() recepieSelected = new EventEmitter<void>();
   // constructor(private recepieService: RecepiesService) { }

  ngOnInit() {
  }

  /*as adding routing*/
  /*onSelected() {
    // this.recepieSelected.emit();
    this.recepieService.recepieCreated.emit(this.recepie);
  }*/
}

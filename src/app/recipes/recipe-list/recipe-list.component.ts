import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Recipe} from '../recipe.model';
import {RecepiesService} from "../recepies.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  // commented bcoz using service for cross component communication
  // @Output() recepieWasSelected = new EventEmitter<Recipe>();
  recepies: Recipe[] = [];
  constructor(private recepieService: RecepiesService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.recepies = this.recepieService.getRecepies();
  }

  // commented bcoz using service for cross component communication
  /*onRecepieSelected(recepieEle: Recipe) {
     this.recepieWasSelected.emit(recepieEle);
  }*/
  onNewRecepie() {
     this.router.navigate(['new'], {relativeTo: this.route});
  }
}

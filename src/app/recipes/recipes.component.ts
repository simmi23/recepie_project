import { Component, OnInit } from '@angular/core';
import {Recipe} from './recipe.model';
import {RecepiesService} from "./recepies.service";

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css'],
  providers: [RecepiesService]
})
export class RecipesComponent implements OnInit {
    selectedRecepie: Recipe;
  constructor(private recepieService: RecepiesService) { }

  ngOnInit() {
    // using observable subject()
    /*this.recepieService.recepieCreated.subscribe(
      (recepie: Recipe) => {this.selectedRecepie = recepie}
    );*/
  }

}

export class Ingredient {
  public name: string;
  /*how many items of that type*/
  public amount: number;

  constructor(name: string, amount: number) {
    this.name = name;
    this.amount = amount;
  }
}

/*-----Shorcut in typescript of above (to declare name there only in constructor)
* export class Ingredient {
*  constructor(public: name: string, public amount: number) {}
* */

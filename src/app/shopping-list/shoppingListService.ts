import {Ingredient} from '../shared/ingredient.model';
import {EventEmitter} from '@angular/core';
import {Subject, Subscription} from "rxjs";

export class ShoppingListService {
  // ingredientsChanged = new EventEmitter<Ingredient[]>();
  ingredientsChanged = new Subject<Ingredient[]>();
  private ingredients: Ingredient[] = [
    new Ingredient('Apples', 5),
    new Ingredient('Tomatoes', 10),
  ];
  getIngredients() {
    return this.ingredients.slice();
    // here we are passing a copy of array so when the list chenges it will not be reflected
    // so to make the reflected changes there are two ways
    // 1) remove slice
    // 2) create Another Event Emitter and subscribe it
  }
  addIngredient(ingredient: Ingredient) {
    this.ingredients.push(ingredient);
   // this.ingredientsChanged.emit(this.ingredients.slice());
    this.ingredientsChanged.next(this.ingredients.slice());
  }
  addIngredientsToShoppingFromRecepie(Iingredients: Ingredient[]) {
    /*for (let Iingredient of ingredients) {
      this.addIngredient(Iingredient);
    }*/
    // ES6 spread operator (... )-turn array of elements into list of elements
    // bcoz push() method is able to handle  multiple objects and it cannot handle array
    // or on the other hand it can but handle array but can pass a single object.
    this.ingredients.push(...Iingredients);
   //  this.ingredientsChanged.emit(this.ingredients.slice());
    this.ingredientsChanged.next(this.ingredients.slice());
  }
}
